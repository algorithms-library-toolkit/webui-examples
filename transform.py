import json
import os
import sys


def transform(filename):
    with open(filename, 'r') as f:
        js = json.load(f)

    for key in js['nodes']:
        obj = js['nodes'][key]

        if obj['name'] == 'Dot Output':
            obj['name'] = 'Output'
            obj['nodeType'] = 'output'
            obj['height'] = 64
            obj['width'] = 81
            obj['params'] = [
                    {
                        'name': 'input',
                        'type': 'abstraction::UnspecifiedType'
                        }
                    ]


    with open(filename, 'w') as f:
        json.dump(js, f)

for i in sys.argv[1:]:
    transform(i)
